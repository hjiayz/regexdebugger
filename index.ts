import {
    AST,
    RegExpParser,
    RegExpValidator,
    parseRegExpLiteral,
    validateRegExpLiteral,
    visitRegExpAST,
} from "regexpp";

import { Pattern } from "regexpp/ast";

///<reference path='./node_modules/immutable/dist/immutable.d.ts'/>
import { Set, Map as IMap } from "immutable";

export type Errors = Error[]
export type Error = {
    regexOffset: number,
    textOffset: number,
}
export type MatchedGroup = IMap<number | string, Matched>;
export type Matched = {
    input: string,
    index: number,
    length: number,
}
export type Global = {
    textOffset: number,
    vars: MatchedGroup,
    errors: Errors,
    l2r: boolean,
}
export type Next = (arg0: Global) => Global

export default class RegexDebugger {
    ast: AST.RegExpLiteral
    pattern: AST.Pattern
    capturingGroupIDS: Map<number, number>;
    constructor(regex: RegExp) {
        this.ast = parseRegExpLiteral(regex);
        this.capturingGroupIDS = buildCapturingGroupIDS(this.ast);
        this.pattern = this.ast.pattern;
        let flags = this.ast.flags;
        if (flags.global) {
            throw new Error("flags global(g) not impl");
        }
        if (!flags.unicode) {
            throw new Error("flags missing unicode(u)");
        }
        if (flags.sticky) {
            throw new Error("flags sticky(y) not impl");
        }
        if (flags.hasIndices) {
            throw new Error("flags hasIndices(d) not impl");
        }
    }
    static create(pat: string, flags: string): RegexDebugger {
        return new RegexDebugger(new RegExp(pat, flags));
    }
    match(s: string): { ok: true, groups: MatchedGroup, err: undefined } | { ok: false, err: Errors, groups: undefined } {
        let textOffset = 0;
        let matched: MatchedGroup = IMap();

        let errors: Errors = [];
        while (true) {
            for (let alt of this.pattern.alternatives) {
                let global: Global = { textOffset, vars: matched, errors: [], l2r: true };
                let ng = this.execAlt(alt, s, global, undefined, (g) => g);
                if (isEmpty(ng)) {
                    let index = textOffset;
                    let length = ng.textOffset - index;
                    let input = s.slice(index, ng.textOffset);
                    matched = ng.vars.set(0, {
                        input,
                        index,
                        length
                    });
                    return { ok: true, groups: matched, err: undefined };
                }
                errors = errors.concat(ng.errors);
            }
            let size = nextChar(s, textOffset)[1] - textOffset;
            if (size === 0) {
                break;
            }
            textOffset = textOffset + size;
        }
        errors = errors.map(fixRegexoffset);
        return { ok: false, err: errors, groups: undefined }
    }
    debug(s: string): Errors {
        let result = this.match(s);
        if (result.ok) {
            return [];
        }
        else {
            return result.err;
        }
    }
    test(s: string): boolean {
        return this.debug(s).length === 0
    }
    execAlt(alt: AST.Alternative, s: string, global: Global, n: number | undefined, next: Next): Global {
        let n1: number;
        if (global.l2r) {
            if (n === undefined) {
                n1 = 0;
            }
            else {
                n1 = n;
            }
            if (n1 >= alt.elements.length) {
                return global;
            }
            return this.execElement(alt.elements[n1], s, global, (newglobal) => this.execAlt(alt, s, newglobal, n1 + 1, next));
        }
        else {
            if (n === undefined) {
                n1 = (alt.elements.length - 1);
            }
            else {
                n1 = n;
            }
            if (n1 < 0) {
                return global;
            }
            return this.execElement(alt.elements[n1], s, global, (newglobal) => this.execAlt(alt, s, newglobal, n1 - 1, next));
        }
    }
    execElement(element: AST.Element, s: string, global: Global, next: Next): Global {
        if (element.type == "Assertion") {
            return this.execAssertion(element, s, global, next);
        } else if (element.type == "CharacterClass") {
            return this.execCharacterClass(element, s, global, next);
        } else if (element.type == "Character") {
            return this.execCharacter(element, s, global, next);
        } else if (element.type == "CharacterSet") {
            return this.execCharacterSet(element, s, global, next);
        } else if (element.type == "Backreference") {
            return this.execBackreference(element, s, global, next);
        } else if (element.type == "Group") {
            return this.execGroup(element, s, global, next);
        } else if (element.type == "CapturingGroup") {
            return this.execCapturingGroup(element, s, global, next);
        } else if (element.type == "Quantifier") {
            return this.execQuantifier(element, s, global, next);
        } else {
            throw new Error("not impl");
        }
    }
    execBackreference(element: AST.Backreference, s: string, global: Global, next: Next): Global {
        let ref = element.ref;

        let v = global.vars.get(ref);
        if (v === undefined) {
            return setError(global, element.start)
        }
        if (global.l2r) {
            if (s.substring(global.textOffset).startsWith(v.input)) {
                return next(setOk(global, v.length));
            }
            else {
                return setError(global, element.start);
            }
        }
        else {
            if (s.substring(0, global.textOffset).endsWith(v.input)) {
                return next(setOk(global, v.length));
            }
            else {
                return setError(global, element.start);
            }
        }
    }
    execQuantifier(element: AST.Quantifier, s: string, global: Global, next: Next): Global {
        let min = element.min;
        let max = element.max;
        let greedy = element.greedy;
        if (greedy) {
            let backStack: Global[] = [];
            let ng1 = global;
            while (backStack.length < max) {
                ng1 = this.execElement(element.element, s, ng1, (g) => g);
                if (isEmpty(ng1)) {
                    backStack.push(ng1);
                }
                else {
                    if (min > backStack.length) {
                        return ng1;
                    }
                    break;
                }
            }
            let ng = global;
            let errors: Errors = [];
            while (backStack.length > 0) {
                let g = backStack.pop() as Global;
                ng = next(g);
                if (isEmpty(ng)) {
                    return ng;
                }

                errors = errors.concat(ng.errors)

                //less than min return errors

                if (backStack.length < min) {
                    return Object.assign({}, global, { errors });
                }
            }
            return next(global);
        }
        else {
            let ng = global;
            for (let i = 0; i < min; i++) {
                ng = this.execElement(element.element, s, global, (g) => g);
                if (!isEmpty(ng)) {
                    return ng
                }
            }
            let errors: Errors = [];
            for (let i = min; i < max; i++) {
                ng = this.execElement(element.element, s, global, (g) => g);
                if (isEmpty(ng)) {
                    let g = next(ng);
                    if (isEmpty(g)) {
                        return g;
                    }
                    else {
                        errors = errors.concat(g.errors);
                    }
                }
            }
            return Object.assign({}, global, { errors });
        }
    }
    execCapturingGroup(element: AST.CapturingGroup, s: string, global: Global, next: Next): Global {
        let errors: Errors = [];
        for (let alt of element.alternatives) {
            let ng = this.execAlt(alt, s, global, undefined, (g) => g);
            if (isEmpty(ng)) {
                let index;
                let length;
                let input;
                if (global.l2r) {
                    index = global.textOffset;
                    length = ng.textOffset - index;
                    input = s.slice(index, ng.textOffset);
                }
                else {
                    index = ng.textOffset;
                    length = global.textOffset - index;
                    input = s.slice(index, global.textOffset);
                }
                let varvalue = {
                    input,
                    index,
                    length
                };
                let id = this.capturingGroupIDS.get(element.start);
                if (typeof id != "number") {
                    throw new Error("is a bug");
                }
                let newvars = global.vars.set(id, varvalue);
                if (element.name) {
                    newvars = global.vars.set(element.name, varvalue);
                }
                return next({
                    textOffset: ng.textOffset,
                    vars: newvars,
                    l2r: global.l2r,
                    errors: [],
                });
            }
            errors = errors.concat(ng.errors);
        }
        return Object.assign({}, global, { errors });
    }
    execGroup(element: AST.Group, s: string, global: Global, next: Next): Global {
        let errors: Errors = [];
        for (let alt of element.alternatives) {
            let ng = this.execAlt(alt, s, global, undefined, (g) => g);
            if (isEmpty(ng)) {
                return next(ng);
            }
            errors = errors.concat(ng.errors);
        }
        return Object.assign({}, global, { errors });
    }
    execCharacterSet(element: AST.AnyCharacterSet | AST.EscapeCharacterSet | AST.UnicodePropertyCharacterSet, s: string, global: Global, next: Next): Global {
        let pat = "^" + element.raw;
        const matcher = new RegExp(pat, this.ast.flags.raw);
        const [ng, ch] = getChar(global, s);
        const matched = matcher.exec(ch);
        if ((!matched) || (matched.length == 0)) {
            return setError(global, element.start)
        }
        return next(ng)
    }
    execCharacterClass(cc: AST.CharacterClass, s: string, global: Global, next: Next): Global {
        let pat = "^" + cc.raw + "";
        const matcher = new RegExp(pat, this.ast.flags.raw);
        const [ng, ch] = getChar(global, s);
        const matched = matcher.exec(ch);
        if ((!matched) || (matched.length == 0)) {
            return setError(global, cc.start)
        }
        return next(ng)
    }
    execCharacter(c: AST.Character, s: string, global: Global, next: Next): Global {
        return this.execCharacterClassElement(c, s, global, next)
    }
    execCharacterClassElement(ccele: AST.CharacterClassElement | AST.AnyCharacterSet, s: string, global: Global, next: Next): Global {
        let pat = "^" + ccele.raw;
        const matcher = new RegExp(pat, this.ast.flags.raw);
        const [ng, ch] = getChar(global, s);
        const matched = matcher.exec(ch);
        if ((!matched) || (matched.length == 0)) {
            return setError(global, ccele.start)
        }
        return next(ng)
    }
    execAssertion(ass: AST.Assertion, s: string, global: Global, next: Next): Global {
        if (ass.kind == "start") {
            return check(global, ass.start, () => {
                return [0, ((global.textOffset == 0) || ((!this.ast.flags.multiline) && (s[global.textOffset - 1] == "\n")))]
            }, next);
        } else if (ass.kind == "end") {
            return check(global, ass.start, () => {
                return [0, ((global.textOffset == s.length) || ((!this.ast.flags.multiline) && (s[global.textOffset] == "\n")))]
            }, next);
        } else if (ass.kind == "lookahead") {
            return this.execLookHead(ass, s, global, next)
        } else if (ass.kind == "lookbehind") {
            return this.execLookBehind(ass, s, global, next)
        } else if (ass.kind == "word") {
            return this.execWord(ass, s, global, next)
        }
        throw new Error("not impl");
    }
    execLookHead(element: AST.LookaheadAssertion, s: string, global: Global, next: Next): Global {
        let errors: Errors = [];
        let setl2r = Object.assign({}, global, { l2r: true });
        for (let alt of element.alternatives) {
            let ng = this.execAlt(alt, s, setl2r, undefined, (g) => g);
            if (isEmpty(ng)) {
                if (element.negate) {
                    return setError(global, element.start);
                }
                return next(global);
            }
            errors = errors.concat(ng.errors);
        }
        if (element.negate) {
            return next(global);
        }
        return Object.assign({}, global, { errors });
    }
    execLookBehind(element: AST.LookbehindAssertion, s: string, global: Global, next: Next): Global {
        let errors: Errors = [];
        let setl2r = Object.assign({}, global, { l2r: false });
        for (let alt of element.alternatives) {
            let ng = this.execAlt(alt, s, setl2r, undefined, (g) => g);
            if (isEmpty(ng)) {
                if (element.negate) {
                    return setError(global, element.start);
                }
                return next(global);
            }
            errors = errors.concat(ng.errors);
        }
        if (element.negate) {
            return next(global);
        }
        return Object.assign({}, global, { errors });
    }
    execWord(element: AST.WordBoundaryAssertion, s: string, global: Global, next: Next): Global {
        let prevIsWord = /\S/.test(getPrevChar(global, s)[1]);
        let nextIsWord = /\S/.test(getChar(global, s)[1]);
        let matched = prevIsWord != nextIsWord != element.negate;
        if (matched) {
            return next(setOk(global, 0));
        } else {
            return setError(global, element.start);
        }
    }
}

function check(global: Global, elementStart: number, ifok: () => [number, boolean], next: Next): Global {
    const [moveOffset, isok] = ifok();
    if (isok) {
        return next({
            textOffset: global.textOffset + moveOffset,
            vars: global.vars,
            l2r: global.l2r,
            errors: []
        })
    }
    return {
        textOffset: global.textOffset,
        vars: global.vars,
        l2r: global.l2r,
        errors: [{
            regexOffset: elementStart,
            textOffset: global.textOffset,
        }]
    }
}

function setError(global: Global, elestart: number): Global {
    return {
        textOffset: global.textOffset,
        vars: global.vars,
        l2r: global.l2r,
        errors: [{
            regexOffset: elestart,
            textOffset: global.textOffset,
        }]
    }
}

function setOk(global: Global, length: number, setvar?: [number | string, Matched]): Global {
    let vars = global.vars;
    if (setvar) {
        if (Array.isArray(setvar)) {
            vars = vars.set(setvar[0], setvar[1]);
        }
        else {
            vars = setvar;
        }
    }
    return {
        textOffset: global.textOffset + length,
        l2r: global.l2r,
        vars,
        errors: []
    }
}

function isEmpty(g: Global): boolean {
    return Array.isArray(g.errors) && (g.errors.length == 0)
}

function buildCapturingGroupIDS(tree: AST.Node): Map<number, number> {
    let list: number[] = [];
    visitRegExpAST(tree, { onCapturingGroupEnter: (node) => list.push(node.start) });
    list.sort((a,b)=>a-b);
    let result = new Map();
    for (let id in list) {
        result.set(list[id], parseInt(id) + 1)
    }
    return result;
}

function getChar(global: Global, s: string): [Global, string] {
    let ch: string;
    let offset: number;
    if (global.l2r) {
        [ch, offset] = nextChar(s, global.textOffset);
    }
    else {
        [ch, offset] = prevChar(s, global.textOffset);
    }
    return [
        Object.assign({}, global, { textOffset: offset }),
        ch
    ]
}
function getPrevChar(global: Global, s: string): [Global, string] {
    let ch: string;
    let offset: number;
    if (global.l2r) {
        [ch, offset] = prevChar(s, global.textOffset);
    }
    else {
        [ch, offset] = nextChar(s, global.textOffset);
    }
    return [
        Object.assign({}, global, { textOffset: offset }),
        ch
    ]
}

function nextChar(s: string, offset: number): [string, number] {
    if (offset == s.length) {
        return ["", s.length]
    }
    if (offset == s.length - 1) {
        return [s.substring(offset), s.length];
    }
    ////0xD800-0xDFFF
    if ((s.charCodeAt(offset) >= 0xD800) && (s.charCodeAt(offset) <= 0xDFFF)) {
        let end = offset + 2;
        return [s.substring(offset, end), end];
    }
    let end = offset + 1
    return [s.substring(offset, end), end];
}
function prevChar(s: string, offset: number): [string, number] {
    ////0xD800-0xDFFF
    if (offset === 1) {
        return [s.substring(0, 1), 0];
    }
    if (offset === 0) {
        return ["", 0];
    }
    if ((s.charCodeAt(offset - 2) >= 0xD800) && (s.charCodeAt(offset - 2) <= 0xDFFF)) {
        let start = offset - 2;
        return [s.substring(start, offset), start];
    }
    let start = offset - 1;
    return [s.substring(start, offset), start];
}

function fixRegexoffset(value: Error): Error {
    return Object.assign({}, value, { regexOffset: value.regexOffset - 1 });
}

// if error return error
// if ok return next(ok)
// if middleware return this.execSth(....,next);