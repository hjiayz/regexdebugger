// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import RegexpDebugger from "./index.js";
import assert from "assert";

let regex=new RegexpDebugger(/[0-9]/u);
const err=(regexOffset,textOffset)=>({regexOffset,textOffset});
const err1 = {
    regexOffset: 0,
    textOffset: 0
  };
assert.deepEqual(regex.debug("a"),  [err1,err(0,1)]);
assert.deepEqual(regex.debug("2"),[]);

regex=new RegexpDebugger(/1/u);

assert.deepEqual(regex.debug("1"),[]);
assert.deepEqual(regex.debug("2"),[err1,err(0,1)]);

regex=new RegexpDebugger(/./u);

assert.deepEqual(regex.debug("1"),[]);
assert.deepEqual(regex.debug("a"),[]);
assert.deepEqual(regex.debug("."),[]);
assert.deepEqual(regex.debug("\n"),[err1,err(0,1)]);

regex=new RegexpDebugger(/123/u);

assert.deepEqual(regex.debug("123"),[]);
assert.deepEqual(regex.debug("12"),[err(2,2),err(0,1),err(0,2)]);
assert.deepEqual(regex.debug("13"),[err(1,1),err(0,1),err(0,2)]);
assert.deepEqual(regex.debug("\n"),[err1,err(0,1)]);

regex=new RegexpDebugger(/^$/u);

assert.deepEqual(regex.debug(""),[]);

regex=new RegexpDebugger(/(a)+/u);

assert.deepEqual(regex.debug("aa"),[]);


regex=new RegexpDebugger(/a*$/u);

assert.deepEqual(regex.debug("a"),[]);

assert.deepEqual(regex.debug("aa"),[]);

assert.deepEqual(regex.debug(""),[]);

assert.deepEqual(regex.debug("ab"),[]);

regex=new RegexpDebugger(/(a)\1/u);

assert.deepEqual(regex.debug("aa"),[]);

regex=new RegexpDebugger(/abc(?=d)/u);

assert.deepEqual(regex.debug("abcd"),[]);
assert.deepEqual(regex.debug("abcx"),[err(6,3),err(0,1),err(0,2),err(0,3),err(0,4)]);

regex=new RegexpDebugger(/abc(?!d)/u);

assert.deepEqual(regex.debug("abcd"),[err(3,3),err(0,1),err(0,2),err(0,3),err(0,4)]);
assert.deepEqual(regex.debug("abcx"),[]);

regex=new RegexpDebugger(/\b/u);

assert.deepEqual(regex.debug(""),[err(0,0)]);
assert.deepEqual(regex.match("x").groups?.get(0)  ,{ input: '', index: 0, length: 0 });


regex=new RegexpDebugger(/\bx/u);

assert.deepEqual(regex.match("x x").groups?.get(0)  ,{ input: 'x', index: 0, length: 1 });
assert.deepEqual(regex.match("  x").groups?.get(0)  ,{ input: 'x', index: 2, length: 1 });

regex=new RegexpDebugger(/(?<=123)abc/u);
assert.deepEqual(regex.debug("123abc")  ,[]);

regex=new RegexpDebugger(/(?<=\1(1234))abc/u);
assert.deepEqual(regex.debug("12341234abc")  ,[]);


regex=new RegexpDebugger(/(?<=1*)abc/u);
assert.deepEqual(regex.debug("   111111111111abc")  ,[]);

regex=new RegexpDebugger(/abc$/u);

assert.deepEqual(regex.debug("  abc\n    add")  ,[]);

regex=new RegexpDebugger(/abc$/um);

assert(!regex.test("  abc\n    add"));

regex=new RegexpDebugger(/💗$/um);

assert.deepEqual(regex.debug("💗 "),[err(2,2),err(0,2),err(0,3)]);

regex=new RegexpDebugger(/(1)(2)/um);
assert.deepEqual(regex.match("12").groups?.get(1)  ,{ input: '1', index: 0, length: 1 });
assert.deepEqual(regex.match("12").groups?.get(2)  ,{ input: '2', index: 1, length: 1 });

regex=new RegexpDebugger(/a(12){0,3}b/um);
assert.deepEqual(regex.debug("ab") ,[]);
assert.deepEqual(regex.debug("a1b") ,[err(10,1),err(0,1),err(0,2),err(0,3)]);
assert.deepEqual(regex.debug("a12b") ,[]);

const examples=[
  ["llo",["hello"]],
  ["^.$",
    [
      "a",
      "3",
      "π",
      "\u2027",
      "\u0085",
      "\v",
      "\f",
      "\u180E",
      "\u{10300}",
      "\n",
      "\r",
      "\u2028",
      "\u2029",
      "\uD800",
      "\uDFFF",
    ]
  ],
  ["[]?",
    [
      ""
    ]
  ],
]

for (let [pat,list] of examples) {
  for(let flags of ["u","um","ums","ui","umi","umsi","us","usi"]) {
    let exp = new RegExp(pat,flags);
    for (let s of list) {
      assert.equal((new RegexpDebugger(exp)).test(s),exp.test(s));
    }
  }
}

"".match(/123/)