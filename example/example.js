// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import RegexDebugger from "regexdebugger";
import assert from "assert";
let regex = new RegexDebugger(/^[a-z][a-zA-Z0-9]+/u);
let result = regex.debug("AA");
assert(result.filter((err) => err.regexOffset != 0)[0].textOffset == 0);