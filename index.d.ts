import { AST } from "regexpp";
import { Map } from "immutable";
export type Errors = Error[];
export type Error = {
    regexOffset: number;
    textOffset: number;
};
export type MatchedGroup = Map<number | string, Matched>;
export type Matched = {
    input: string;
    index: number;
    length: number;
};
export type Global = {
    textOffset: number;
    vars: MatchedGroup;
    errors: Errors;
    l2r: boolean;
};
export type Next = (arg0: Global) => Global;
export default class RegexDebugger {
    ast: AST.RegExpLiteral;
    pattern: AST.Pattern;
    capturingGroupIDS: Map<number, number>;
    constructor(regex: RegExp);
    static create(pat: string, flags: string): RegexDebugger;
    match(s: string): {
        ok: true;
        groups: MatchedGroup;
        err: undefined;
    } | {
        ok: false;
        err: Errors;
        groups: undefined;
    };
    debug(s: string): Errors;
    test(s: string): boolean;
    execAlt(alt: AST.Alternative, s: string, global: Global, n: number | undefined, next: Next): Global;
    execElement(element: AST.Element, s: string, global: Global, next: Next): Global;
    execBackreference(element: AST.Backreference, s: string, global: Global, next: Next): Global;
    execQuantifier(element: AST.Quantifier, s: string, global: Global, next: Next): Global;
    execCapturingGroup(element: AST.CapturingGroup, s: string, global: Global, next: Next): Global;
    execGroup(element: AST.Group, s: string, global: Global, next: Next): Global;
    execCharacterSet(element: AST.AnyCharacterSet | AST.EscapeCharacterSet | AST.UnicodePropertyCharacterSet, s: string, global: Global, next: Next): Global;
    execCharacterClass(cc: AST.CharacterClass, s: string, global: Global, next: Next): Global;
    execCharacter(c: AST.Character, s: string, global: Global, next: Next): Global;
    execCharacterClassElement(ccele: AST.CharacterClassElement | AST.AnyCharacterSet, s: string, global: Global, next: Next): Global;
    execAssertion(ass: AST.Assertion, s: string, global: Global, next: Next): Global;
    execLookHead(element: AST.LookaheadAssertion, s: string, global: Global, next: Next): Global;
    execLookBehind(element: AST.LookbehindAssertion, s: string, global: Global, next: Next): Global;
    execWord(element: AST.WordBoundaryAssertion, s: string, global: Global, next: Next): Global;
}

//# sourceMappingURL=index.d.ts.map
