
# online

>https://hjiayz.gitlab.io/regexdebugger

# example
```js
    import RegexDebugger from "regexdebugger";
    import assert from "assert";
    let regex = new RegexDebugger(/^[a-z][a-zA-Z0-9]+/u);
    let result = regex.debug("AA");
    assert(result.filter((err) => err.regexOffset != 0)[0].textOffset == 0);
```